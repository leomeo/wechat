
function receiveWechatXml(req, res) {
    const jsonData = req.body;
    console.log(jsonData);
    const unionid = jsonData['xml']['fromusername'];
    io.emit('openid', jsonData);
    res.send(" "); //返回给微信服务器空字符串 阻止其重复发送请求（如果无回复，微信将再重试两次）
  }
  module.exports = receiveWechatXml;