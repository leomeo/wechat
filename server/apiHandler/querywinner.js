function querywinner(str) {
  var strarray = str.split(","); // 格式 ["12212:122","12332:231","12122:12"];
  var newarray = [];

  for (var i = 0; i < strarray.length; i++) {
    newarray[i] = (strarray[i].split(':'));
    for (var j = 0; j < 2; j++) { //数组长度为2
      newarray[i][j] = Math.abs(newarray[i][j]);
    }
  } // 格式[[100024, 20],[100025, 60],[100026, 20],[100032, 20]]

  var array2 = [];
  for (let i = 0; i < newarray.length; i++) {
    array2[i] = newarray[i][1];
  }
  var index = array2.indexOf(Math.max.apply(Math, array2));
  return newarray[index][0];
};

module.exports = querywinner;
