const express = require("express");
const app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const fs = require("fs");
const path = require("path");
const promise_mysql = require("promise-mysql");
const wechatcheck = require('./apiHandler/wechatcheck');
const xmlparser = require("express-xml-bodyparser");
const get_access_token_func = require("./apiHandler/get_access_token");
const dbHandler = require("./dbHandler/dbHandler");
const setWechatMenu = require('./apiHandler/setWechatMenu');
const cookie = require("cookie-parser");
const checkCookie = require("./apiHandler/checkCookie");
const bodyParser = require('body-parser');
const eventproxy = require("eventproxy");
const ep = new eventproxy();
const Format = require("./apiHandler/dateformat");
const detailquery = require("./apiHandler/detailquery");
const detail4 = require("./apiHandler/detail4");
const detail5 = require("./apiHandler/detail5");
const removebind = require('./apiHandler/removebind');
const tixianorder = require("./apiHandler/tixianorder");
const agentclub = require("./apiHandler/agentclub");
const deleterows = require("./apiHandler/deleterows");
const myagent = require("./apiHandler/myagent");
const receiveWechatXml = require("./apiHandler/receiveWechatXml");
const all = require("./apiHandler/all");
const test = require('./apiHandler/test');
const recharge = require("./apiHandler/recharge");
const diamond = require("./apiHandler/diamond");
const earningquery = require("./apiHandler/earningquery");
const config = require("./config");
const SqlString = require("sqlstring");

app.use(cookie());
app.use(express.json());

app.use(xmlparser());
app.use(express.static("public"));
app.use(cookie());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

server.listen(80, function(err, res){
console.log("server  running  at  :443 ");

});

app.get("/",function(req, res){
console.log('copy  that');
});

//get_access_token_func();
//每小時請求一次access_ token，以保持access_token 一直可用
setInterval(get_access_token_func, 60 * 60 * 1000);

//微信绑定url地址 验证通道
 app.get('/wechat', wechatcheck);
app.get("/test", function (req, res) {
  console.log(req.query.openid)
});

//所有请求都允许跨域
app.all("*", function (req, res, next) {
  all(req, res, next)
});

// app.get('/', checkCookie(req,res)); 已证明cookie在微信中不可用，废弃

//用户点击充值菜单返回的默认页面
app.get("/download", function (req, res) {
  res.sendFile(path.resolve(__dirname, "./public/views/download.html"));
})


//充值页面
app.get("/recharge_page", function(req, res){
 res.sendFile(path.resolve(__dirname, "./public/views/recharge1.html"));

});


//当 用户点击菜单 微信将发送 一个包括openid的xml数据到此通道
app.post('/', function (req, res) {
  receiveWechatXml(req, res)
});

//测试代码
app.post('/test', function (req, res) {
  test(req, res)
});

//充值
app.get('/recharge', function (req, res) {
  recharge(req, res)
});

//代理查询 钻石流水
app.get("/diamond", function (req, res) {
  diamond(req, res)
});

//代理查询充值获得钻石明细
app.get("/detail1", function (req, res) {
  detailquery(req, res, "充值");
});

//代理查询活动获得钻石明细
app.get("/detail2", function (req, res) {
  detailquery(req, res, "活动");
});

//代理查询任务获得钻石明细
app.get("/detail3", function (req, res) {
  detailquery(req, res, "任务");
});

//代理查询个人开房消耗钻石明细
app.get("/detail4", function (req, res) {
  detail4(req, res)
});

//代理查询俱乐部开房消耗钻石明细
app.get("/detail5", function (req, res) {
  detail5(req, res)
});

//代理收入查询
app.get("/earningquery", function (req, res) {
  earningquery(req, res)
});

//代理查询  我的代理
app.get('/myagent', function (req, res) {
  // console.log("success");
  myagent(req, res);
});

//代理俱乐部
app.get("/agent_club", function (req, res) {
  agentclub(req, res)
});

//客服 解除绑定  如果成功则往sys_dailibind——change——log表中 添加记录   若操作为解除代理  则 operate为零 
app.get("/removebind", function (req, res) {
  removebind(req, res)
});

//客服  提现订单
app.get('/tixianorder', function (req, res) {
  tixianorder(req, res)
});

//客服 提现订单页面  客服点击删除记录的通道 
app.get("/deleterows", function (req, res) {
  deleterows(req, res)
});

//客服 代理信息
app.get('/kf_myagent', function (req, res) {
  let sql = ' select `invitecode`, `wxunionid`, `nick`,`daililevel` from roledata where daililevel <>0 ';
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    console.log(rows);
    res.json({
      "result": rows
    });
  });
});

//客服 代理降级 （如果是一级 则能降级）
app.get('/jiangji', function (req, res) {
  var invitecode = req.query.invitecode;
  console.log(invitecode);
  var sql = "update `roledata` SET  `daililevel` = 2 where `invitecode`=" + invitecode;
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    res.status(200).json({
      "result": "代理降级操作成功"
    });
  });
});

//客服 代理升级
app.get('/shengji', function (req, res) {
  var invitecode = req.query.invitecode;
  var sql = "update `roledata` SET  daililevel = 1 where invitecode=" + invitecode;
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    res.status(200).json({
      "result": "代理升级操作成功"
    });
  });
});

//客服 代理解除
app.get('/jiechu', function (req, res) {
  var invitecode = req.query.invitecode;
  var sql = "update `roledata` SET  daililevel = 0 where invitecode=" + invitecode;
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    res.status(200).json({
      "result": "代理解除操作成功"
    });
  });
});

//客服  添加玩家 为 2级代理
app.get('/addagent', function (req, res) {
  var invitecode = req.query.invitecode;
  var sql = "update `roledata` SET  daililevel = 2 where invitecode='" + invitecode + "'";
  console.log(sql);
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    if (rows["changedRows"] == 0) {
      res.json({
        "result": "添加失败"
      });
    } else {
      var sql = "insert into `sys_dailibind_change_log`(`invitecode`, `daili`, `operate`, `unionkefu_id`, `create_time`) values(" + invitecode + ","+ "0" + " , '4'" + " , 001 " + "  , NOW())";
   dbHandler(sql,  function(err, rows){
if(err) throw  err;
    res.json({
      "result": "添加成功"
    });
   });
    }
  });
});

// 客服 添加新代理 输入游戏号 验证通道
app.get('/isagent', function (req, res) {
  var invitecode = req.query.invitecode;
  console.log(invitecode);
  var sql = " select  `nick`,`daililevel` from roledata where invitecode ='" + invitecode + "'";
  console.log(sql);
  dbHandler(sql, function (err, rows) {
    console.log(rows);
    if (err) throw err;
    if (rows == '') {
      res.status(400).json({
        "result": {
          "message": "该ID玩家不存在",
          'data': ''
        }
      });
    } else {
      var nick = rows[0].nick;
        console.log(rows);
        if (rows[0].daililevel == 0) {
          console.log("还不是代理 可以成为代理");
          res.status(200).json({
            "result": {
              "message": "该玩家还不是代理",
              "data": {
                "nick": nick
              }
            }
          });
        } else {
          console.log("已经是代理 代理id是", rows[0].daililevel, nick);
          let data = {
            "daililevel": rows[0].daililevel,
            "nick": nick
          };
          let result = {
            "result":{
              "message": "该玩家已经是代理",
              "data": data
            }
          };
          res.status(400).json(result);
        }
    }
  })
});




//  客服 俱乐部
app.get('/kf_club', function (req, res) {
  var uid = req.query.uid;
  var sql = "  SELECT  c.clubid, c.clubname, c.clubmaster, r.nick,  c.clubmaster FROM roledata AS r,club AS c where c.clubmaster = r.uid  and  c.isdissolve = 0 ";
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    res.json({
      "result": rows
    });
  });
});

//客服 创建俱乐部
app.get("/addclub", function (req, res) {
  let clubname = req.query.clubname;
  let clubmaster_id = req.query.clubmaster_id;
  let sql1 = "select *   from  roledata where  `invitecode` =" + clubmaster_id;
  dbHandler(sql1, function (err, rows) {
    if (err) throw err;
    console.log(rows);

  });

});


// 客服 解散俱乐部
app.get('/dissolve', function (req, res) {
  let clubid = req.query.clubid;
  let guild_master = req.query.clubmaster;
  console.log(clubid, guild_master);
  let sql1 = "UPDATE  `club` SET `isdissolve` = 1 where `clubid` = " + clubid; //修改 club表中的 是否解散字段
  let sql2 = "select `wxunionid`   from  roledata where  `clubid` =" + clubid; // 查询所有这个俱乐部的人（会长和会员）
  // let  sql3  = "insert "+rows[i].wxunionid+  sys_guild_change_log +"";            //把上一步查到的所有人插入到 修改记录表中
  let sql4 = "UPDATE  `roledata` SET `clubid` = 0 where clubid = " + clubid; // 把roledata表中 所有的相关人 的clubid去掉
  // insert into  sys_guild_change_log (unionid,guild_id,guild_master,operate,create_time,uinionkefu_id)values(?,clubid,guild_master,0,NOW()  ,001);
  // NOW() 
  var connection;

});



//客服 解除绑定页面  判定是否是玩家|是否绑定了代理
app.get('/isplayer', function (req, res) {
  let invitecode = req.query.invitecode;
  let sql = "select * from `roledata` where invitecode = '" + invitecode + "'";
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    if (rows == '') {
      console.log("该ID玩家不存在");
      res.status(400).json({
        "result": "该ID玩家不存在"
      });


    } else {

      console.log(rows[0].dailibind);
      if (rows[0].dailibind == 0) {
        console.log(rows[0].dailibind);
        res.status(400).json({
          "result": "该玩家还没有绑定代理"
        });
      } else {
        console.log("--------------------");
        console.log(rows[0].nick);
        console.log("--------------------");
        let result = [];
        let dailiid = rows[0].dailibind;
        let nick = rows[0].nick;
        let sql = "select *  from  roledata where  invitecode  = " + dailiid;
        dbHandler(sql, function (err, rows) {
          if (err) throw err;

          result.push(nick, dailiid, rows[0].nick);
          res.status(200).json(result);


        });


      }
    }
  });

});


// 客服  解除代理绑定页面的 查询 sys_dailibind_change_log
app.get('/dailibind_change_query', function (req, res) {
  var uid = req.query.uid;
  let sql = "select * from `sys_dailibind_change_log`";
  dbHandler(sql, function (err, rows) {
    if (err) throw err;
    if (rows.length > 0) {
      for (let i = 0; i < rows.length; i++) {
        console.log(rows[i]['create_time']);
        rows[i]['create_time'] = rows[i].create_time.Format('yyyy-MM-dd');
      }
      console.log(rows);
      res.json({
        "result": rows
      });
    } else {
      res.status(400).json({
        "result": "没有查询到数据"
      });
    }
  })
});

// 任命代理
app.get('/renmingagent', function(req, res){
 let  invitecode = req.query.invitecode;
  let sql = "UPDATE `roledata` SET  `daililevel` = 2 where `invitecode`=" + invitecode;

  var promise1  =  new  Promise(
    function(resolve,reject){
      dbHandler(sql, function(err, res){
        if(err) {reject(err)}
        else{resolve(res)}
      });
    }
  ).then(
function(){
  var promise2  = new  Promise(
    function(resolve,reject){
      let  sql  ="insert into `sys_dailibind_change_log`(`invitecode`, `daili`, `operate`, `unionkefu_id`, `create_time`) values(" + invitecode + ","+ "0" + " , '4'" + " , 001 " + "  , NOW())";;
      dbHandler(sql, function(err, res){
        if(err) {reject(err)}
        else{resolve(res)}
      });
    }
  )
  return  promise2;
}
  ).then(
    function(data){
      console.log(data);
      res.status(200).json({"result":"操作成功"});
    }).catch(
      function(err){
      console.log(err);
      res.status(400).json({"result":"操作失败"});
    });
});

//earningquery 查询 sys_order表
app.post('/query_sys_order', function(req, res){
  var uid  =  req.body.owner;
  var obj = req.body.month
  console.log("body");
  console.log(obj);
  console.log("body");


  var aaa = new eventproxy();
  var  isshow =[];
  aaa.after('dbquery2', obj.length, function (list) {
      for(var i=0; i<list.length ;i++){
     if(list[i].length >0 && list[i]['month'] != ''){
      isshow[i] =0;
     }else{
      isshow[i] = 1;
     }
     
      }

   // if(list[i].length ==1){
        //    isshow[i] = 1;
         
        // }else{ isshow[i] =0;}
        console.log(list[i]);



      
 console.log(isshow);
    res.status(200).json({
      "result": isshow
    });
  });

  for (var i = 0; i < obj.length; i++) {
    (function (num) {
      var  sql  = "select `month`  from  `sys_order` where  `dailiid` ='" + uid + "' and `month` ='" + obj[i]["month"]+"'";
      console.log(sql);
      dbHandler(sql, aaa.group('dbquery2'))
    })(i); 
  };

});


//代理 收入查询页面 提现的按钮
app.get('/tixian', function(req, res){
var  uid  = req.query.uid;
var  themonth = req.query.month;
var  cash = req.query.cash;
console.log(themonth);
var sql = "insert into `sys_order`(`create_time`, `dailiid`, `cash`, `month`) values(NOW()," + uid +","+ cash + ",'"+ themonth  +"')" ; 
console.log(sql);
dbHandler(sql, function(err, rows){
  if(err) {
    res.status(400).json({"result":"提现失败"});
    throw  err;}
  res.status(200).json({"result":"提现成功，等待客服处理"});
});
});


function hello1(q, s){
  console.log(q);
  res.send(q);
}
app.get('/test', function hello(req, res){
  console.log(req);
} );
