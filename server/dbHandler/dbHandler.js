
const mysql = require("mysql");
const path   =  require('path');
const   fs  =  require('fs');
const  config  =  JSON.parse( fs.readFileSync( path.resolve( __dirname, '../config.json')));
console.log(config);

var connection = mysql.createPool({
    host: config.mysql.host,
    user: config.mysql.user,
    database: config.mysql.database,
    password: config.mysql.password
});


function dbHandler(sql,callback){
    
        connection.query(sql, function (err,rows) {
            callback(err,rows);
 
        });
  
}

module.exports = dbHandler;
