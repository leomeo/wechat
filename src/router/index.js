import Vue            from 'vue'
import Router         from 'vue-router'
import HelloWorld     from '@/components/HelloWorld'
import  downloadpage  from '@/components/downloadpage'        //下载游戏页面
import  playerpage  from '@/components/playerpage'            //玩家页面
import  agentpage  from '@/components/agentpage'              //代理 后台管理页面
import  agent_club  from '@/components/agent_club'            //代理 俱乐部页面
import  agent_agentinfo  from '@/components/agent_agentinfo'  //代理 代理信息页面
import  earningquery  from '@/components/earningquery'        //代理 收入查询页面

import  diamond  from '@/components/diamond'                  //代理 钻石流水 页面
import  detail1  from '@/components/detail1'                  //代理 充值获得 查询明细页面
import  detail2  from '@/components/detail2'                  //代理 活动获得 查询明细页面
import  detail3  from '@/components/detail3'                  //代理 大厅开房 查询明细页面
import  detail4  from '@/components/detail4'                  //代理 个人开房 查询明细页面
import  detail5  from '@/components/detail5'                  //代理 俱乐部开房 查询明细页面

import  kf_page  from '@/components/kf_page'                  //客服 后台管理页面
import  kf_agentinfo  from '@/components/kf_agentinfo'        //客服 代理信息页面
import  kf_club  from '@/components/kf_club'                  //客服 俱乐部页面
import  kf_removebind  from '@/components/kf_removebind'      //客服 解除绑定
import  kf_tixianorder  from '@/components/kf_tixianorder'    //客服 体现订单

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'agentpage',
      component:  agentpage
    
    },
    {
      path: '/kf_page',
      name: 'kf_page',
      component: kf_page
    },
  
    {
      path: '/playerpage',
      name: 'playerpage',
      component: playerpage
    },
    {
      path: '/kf_agentinfo',
      name: 'kf_agentinfo',
      component: kf_agentinfo
    },
    {
      path: '/downloadpage',
      name: 'downloadpage',
      component: downloadpage
    },
    {
      path: '/agent_club',
      name: 'agent_club',
      component: agent_club
    },
    {
      path: '/kf_club',
      name: 'kf_club',
      component: kf_club
    },
    {
      path: '/diamond',
      name: 'diamond',
      component: diamond
    },
    {
      path: '/detail1',
      name: 'detail1',
      component: detail1
    },
    {
      path: '/detail2',
      name: 'detail2',
      component: detail2
    },
    {
      path: '/detail3',
      name: 'detail3',
      component: detail3
    },
    {
      path: '/detail4',
      name: 'detail4',
      component: detail4
    },
    {
      path: '/detail5',
      name: 'detail5',
      component: detail5
    },
    {
      path: '/earningquery',
      name: 'earningquery',
      component: earningquery
    },
    {
      path: '/kf_removebind',
      name: 'kf_removebind',
      component: kf_removebind
    },
    {
      path: '/kf_tixianorder',
      name: 'kf_tixianorder',
      component: kf_tixianorder
    },
    {
      path: '/agent_agentinfo',
      name: 'agent_agentinfo',
      component: agent_agentinfo
    }
    
  ]
})  
